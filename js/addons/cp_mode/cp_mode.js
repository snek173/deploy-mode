$(document).ready(function() {

    //$(".main-content-product select").minimalect();
    $(".main-content-product select").minimalect({ theme: "bubble", placeholder: "Choose a Size", contenteditable: false });


    $(".cp_visible__mobile_section__menu .cp_mode-menu__items>li>div").css("display","none");

    $(".cp_mode_tabsbox h3").click(function(){
        $(".cp_mode_tabsbox .cp_mode__tab-list-title i").removeClass().addClass("fa fa-angle-down");
        $(this).children("i").removeClass().addClass("fa fa-angle-up");
        $(".cp_mode_tabsbox > h3 + div").hide();
        $(this).next("div").show();
    });


    $(".cp_mode-menu__item-toggles").click(function(event){
            $(this).next().toggle(400);
            event.preventDefault();
            return false;
        });

    $("#showLeft").click(function(event){
        $(".cp_visible__mobile_section__menu").toggleClass("activeBlock");
        $("#tygh_container").toggleClass("activeConteiner");
        $(this).hide();
        return false;
    });
    $("#hidenLeft").click(function(event){
        $(".cp_visible__mobile_section__menu").toggleClass("activeBlock");
        $("#tygh_container").toggleClass("activeConteiner");
        $("#showLeft").show();
        return false;
    });

});
