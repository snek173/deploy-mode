<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/


/*$schema['addons/cp_mode/blocks/wrappers/cp_mode_mainbox_text_center_or_line.tpl'] = array (
    'settings' => array(
        'cp_mode_item_align' => array (
            'type' => 'checkbox',
            'values' => array(
                    'left' => 'cpml',
                    'center' => 'cpmc',
                    'right' => 'cpmr',
            ),
            'default_value' => 'cpmc'
        ),
    ),
);
*/
$schema['addons/cp_mode/blocks/products/products_scroller.tpl'] = array (
        'settings' => array(
            'cp_mode_visiblity_size' => array (
                'type' => 'checkbox',
                'default_value' => 'Y'
            ),
            'show_price' => array (
                'type' => 'checkbox',
                'default_value' => 'Y'
            ),
            'enable_quick_view' => array (
                'type' => 'checkbox',
                'default_value' => 'N'
            ),
            'not_scroll_automatically' => array (
                'type' => 'checkbox',
                'default_value' => 'N'
            ),
            'scroll_per_page' =>  array (
                'type' => 'checkbox',
                'default_value' => 'N'
            ),
            'speed' =>  array (
                'type' => 'input',
                'default_value' => 400
            ),
            'pause_delay' =>  array (
                'type' => 'input',
                'default_value' => 3
            ),
            'item_quantity' =>  array (
                'type' => 'input',
                'default_value' => 5
            ),
            'thumbnail_width' =>  array (
                'type' => 'input',
                'default_value' => 80
            ),
            'outside_navigation' => array (
                'type' => 'checkbox',
                'default_value' => 'Y'
            )
            ),
            'bulk_modifier' => array (
                'fn_gather_additional_products_data' => array (
                    'products' => '#this',
                    'params' => array (
                        'get_icon' => true,
                        'get_detailed' => true,
                        'get_options' => true,
                    ),
                ),
            ),
);
$schema['addons/cp_mode/blocks/static_templates/template_url.tpl'] = array (
        'settings' => array(
            'cp_mode_input_name_url' => array (
                'type' => 'input',
                'default_value' => 'Cart-Power'
            ),
            'cp_mode_input_url' => array (
                'type' => 'input',
                'default_value' => 'http://cart-power.ru'
            ),
            'cp_mode_input_icon' => array (
                'type' => 'input',
                'default_value' => 'fa fa-heart-o'
            ),
            'cp_mode_tsetting' => array (
                'type' => 'selectbox',
                'values' => array(
                        'Q' => "cp_mode_icon",
                        'T' => "cp_mode_name",
                        'B' => "cp_mode_icon_name",
                ),
                'default_value' => 'T'
            ),
        ),
);
$schema['addons/cp_mode/blocks/static_templates/cp_mode_search.tpl'] = array (
        'settings' => array(
            'cp_mode_input_icon' => array (
                'type' => 'input',
                'default_value' => 'fa fa-search'
            )
        ),
);
$schema['addons/cp_mode/blocks/menu/text_link_dropdown.tpl'] = array (
        'settings' => array(
            'cp_mode_input_icon' => array (
                'type' => 'input',
                'default_value' => 'fa fa-life-ring'
            )
        ),
);
$schema['addons/cp_mode/blocks/menu/cp_mode_dropdown_horizontal.tpl'] = array (
        'settings' => array (
            'cp_mode_visiblity_logo' => array (
                'type' => 'checkbox',
                'default_value' => 'Y'
            ),
            'dropdown_second_level_elements' => array (
                'type' => 'input',
                'default_value' => '12'
            ),
            'dropdown_third_level_elements' => array (
                'type' => 'input',
                'default_value' => '6'
            ),
        ),
);
$schema['addons/cp_mode/blocks/categories/cp_mode_categories_dropdown_horizontal.tpl'] = array (
        'params' => array (
            'plain' => '',
            'request' => array (
                'active_category_id' => '%CATEGORY_ID%',
            ),
        ),
        'settings' => array(
            'right_to_left_orientation' => array (
                'type' => 'checkbox',
                'default_value' => 'N'
            )
        ),
        'fillings' => array('full_tree_cat', 'dynamic_tree_cat', 'subcategories_tree_cat'),
);
return $schema;
