<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Enum\ProductTracking;
use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_cp_mode_in_wishlist($product_id)
{
    if (empty($_SESSION['wishlist'])) {
        return false;
    }
    
    foreach ($_SESSION['wishlist']['products'] as $key => $product) {
        if (!empty($product['product_id']) && $product['product_id'] == $product_id) {
            return $key;
        }   
    } 


    return false;
}

function fn_cp_mode_get_categories($params, $join, $condition, &$fields, $group_by, $sortings, $lang_code) {

    $fields[] = '?:categories.cp_mode_cat_link_class';
    $fields[] = '?:categories.cp_mode_cat_nBlock';

}
