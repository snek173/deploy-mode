<?php

use Tygh\Enum\ProductTracking;
use Tygh\Registry;
use Tygh\Storage;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

Tygh::$app['session']['wishlist'] = isset(Tygh::$app['session']['wishlist']) ? Tygh::$app['session']['wishlist'] : array();
$wishlist = & Tygh::$app['session']['wishlist'];
Tygh::$app['session']['continue_url'] = isset(Tygh::$app['session']['continue_url']) ? Tygh::$app['session']['continue_url'] : '';
$auth = & Tygh::$app['session']['auth'];



if ($mode == 'add_to_wish_list') {
    if (!empty($_REQUEST['product_id'])) {
         if (empty($wishlist)) {
            $wishlist = array(
                'products' => array()
            );
        }

        $prev_wishlist = $wishlist['products'];

        $p_data['product_data'] = array(
            $_REQUEST['product_id'] => array(
                'amount' => 1,
                'product_id' => $_REQUEST['product_id']
            )
        );
    
        $product_ids = fn_add_product_to_wishlist($p_data['product_data'], $wishlist, $auth);

        fn_save_cart_content($wishlist, $auth['user_id'], 'W');

        $product_cnt = 0;
        $added_products = array();
        foreach ($wishlist['products'] as $key => $data) {
            if (empty($prev_wishlist[$key]) || !empty($prev_wishlist[$key]) && $prev_wishlist[$key]['amount'] != $data['amount']) {
                $added_products[$key] = $data;
                $added_products[$key]['product_option_data'] = fn_get_selected_product_options_info($data['product_options']);
                if (!empty($prev_wishlist[$key])) {
                    $added_products[$key]['amount'] = $data['amount'] - $prev_wishlist[$key]['amount'];
                }
                $product_cnt += $added_products[$key]['amount'];
            }
        }

        if (defined('AJAX_REQUEST')) {
            if (!empty($added_products)) {
                foreach ($added_products as $key => $data) {
                    $product = fn_get_product_data($data['product_id'], $auth);
                    $product['extra'] = !empty($data['extra']) ? $data['extra'] : array();
                    $product['selected_options'] = $data['product_options'];
                    fn_gather_additional_product_data($product, true, true);
                    $added_products[$key]['product_option_data'] = fn_get_selected_product_options_info($data['product_options']);
                    $added_products[$key]['display_price'] = $product['price'];
                    $added_products[$key]['amount'] = empty($data['amount']) ? 1 : $data['amount'];
                    $added_products[$key]['main_pair'] = fn_get_cart_product_icon($data['product_id'], $data);
                }
                Tygh::$app['view']->assign('added_products', $added_products);

                if (Registry::get('settings.General.allow_anonymous_shopping') == 'hide_price_and_add_to_cart') {
                    Tygh::$app['view']->assign('hide_amount', true);
                }

                $title = __('product_added_to_wl');
                $msg = Tygh::$app['view']->fetch('addons/wishlist/views/wishlist/components/product_notification.tpl');
                fn_set_notification('I', $title, $msg, 'I');
            } else {
                if ($product_ids) {
                    fn_set_notification('W', __('notice'), __('product_in_wishlist'));
                }
            }
        } else {
            unset($_REQUEST['redirect_url']);
        }

        die();   



        
    }
} elseif ($mode == 'delete' && !empty($_REQUEST['cart_id']) && !empty($_REQUEST['no_redirect'])) {
    fn_delete_wishlist_product($wishlist, $_REQUEST['cart_id']);

    fn_save_cart_content($wishlist, $auth['user_id'], 'W');

    fn_set_notification('W', __('notice'), __('product_removed'));
    exit;

}
