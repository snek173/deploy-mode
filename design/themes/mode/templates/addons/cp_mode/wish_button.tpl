{$cart_id = fn_cp_mode_in_wishlist($product.product_id)}


 {assign var="return_current_url" value=$config.current_url|escape:url}
<a title="{__("cp_mode_logins")}" class="cm-submit-link {if $cart_id}already-in-wishlist{/if} {if !$smarty.session.auth.user_id}cm-dialog-opener cm-dialog-auto-size{/if}"
    {if !$smarty.session.auth.user_id}
        href='{"auth.login_form?return_url=`$return_current_url`"|fn_url}' data-ca-target-id="open_id_ajax"
    {/if}
onclick='
{if $smarty.session.auth.user_id}
       
       if ($(this).hasClass("already-in-wishlist")) {
        $.ceAjax("request", "{fn_url("wishlist.delete?cart_id={$cart_id}&no_redirect=true")}",{ldelim}result_ids:"tygh_container",full_render:true{rdelim});
          
           $(this).removeClass("already-in-wishlist");
           $(this).html("<i class=\"fa fa-heart-o\"></i>");
       } else {
          $.ceAjax("request", "{fn_url("wishlist.add_to_wish_list?product_id={$product.product_id}")}",{ldelim}result_ids:"tygh_container",full_render:true{rdelim});
           $(this).addClass("already-in-wishlist");
           $(this).html("<i class=\"fa fa-heart-o\"></i>");
       }
       return false;
{/if}
       '>


{if $cart_id}
    <i class="fa fa-heart-o  already-in-wishlist__ico"></i>
{else}
    <i class="fa fa-heart-o"></i>
{/if}

</a>