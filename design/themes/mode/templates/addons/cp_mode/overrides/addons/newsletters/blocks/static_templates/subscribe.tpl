{** block-description:tmpl_subscription **}
{if $addons.newsletters}
<div class="ty-footer-form-block">
    <form action="{""|fn_url}" method="post" name="subscribe_form">
        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
        <input type="hidden" name="newsletter_format" value="2" />
        <div class="ty-footer-form-block__form ty-control-group ty-input-append subscribe_form--footer">
            <label class="cm-required cm-email hidden" for="subscr_email{$block.block_id}">{__("email")}</label>
            <input type="text" name="subscribe_email" id="subscr_email{$block.block_id}" size="20" value="{__("enter_email")}" class="cm-hint ty-input-text" />
            {include file="buttons/button.tpl" but_text="{__("cp_mode_subscribe_btn_go")}" but_name="newsletters.add_subscriber" but_meta="subscribe_btn_go" alt=__("go")}
        </div>
    </form>
</div>
{/if}