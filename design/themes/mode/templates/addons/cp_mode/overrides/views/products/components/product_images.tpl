{* $Id$ *}

{assign var="tw_size" value="80"}
{assign var="th_size" value="106"}

{if $product.main_pair.icon || $product.main_pair.detailed}
    {assign var="image_pair_var" value=$product.main_pair}
{elseif $product.option_image_pairs}
    {assign var="image_pair_var" value=$product.option_image_pairs|reset}
{/if}

{if $image_pair_var.image_id == 0}
    {assign var="image_id" value=$image_pair_var.detailed_id}
{else}
    {assign var="image_id" value=$image_pair_var.image_id}
{/if}

{if $image_pair_var.detailed.image_path}
    {assign var="link_class_value" value="cloud-zoom"}
{/if}

<div id="mode_product_image" class="slider-pro{if !$product.image_pairs} slider-pro__img--alone{/if}">
    <div class="sp-slides">

        <div class="sp-slide">
            {include file="common/image.tpl" obj_id="`$product.product_id`_`$image_id`" images=$image_pair_var object_type="detailed_product" show_thumbnail="Y" image_width=$image_width image_height=$image_height rel="product" wrap_image=true link_class=$link_class_value class="sp-image cloud-zoom" link_class="fancybox cm-previewer"}
        </div>

        {foreach from=$product.image_pairs item="image_pair"}
            {if $image_pair}

                <div class="sp-slide">
                    {if $image_pair.image_id == 0}
                        {assign var="image_id" value=$image_pair.detailed_id}
                    {else}
                        {assign var="image_id" value=$image_pair.image_id}
                    {/if}
                    {include file="common/image.tpl" images=$image_pair object_type="detailed_product" link_class="hidden" show_thumbnail="Y" detailed_link_class="hidden" obj_id="`$product.product_id`_`$image_id`" image_width=$settings.Thumbnails.product_details_thumbnail_width image_height=$settings.Thumbnails.product_details_thumbnail_height rel="product" wrap_image=true class="sp-image cloud-zoom" link_class="fancybox cm-previewer"}
                </div>

            {/if}
        {/foreach}


    </div>

    <div class="sp-thumbnails">
        {if $image_pair_var && $product.image_pairs}

            {strip}
                {if $image_pair_var.image_id == 0}
                    {assign var="img_id" value=$image_pair_var.detailed_id}
                {else}
                    {assign var="img_id" value=$image_pair_var.image_id}
                {/if}
                {include file="common/image.tpl" images=$image_pair_var object_type="detailed_product" class="sp-thumbnail" image_width=$tw_size image_height=$th_size show_thumbnail="Y" show_detailed_link=false obj_id="`$product.product_id`_`$img_id`_mini" wrap_image=false}
                {foreach from=$product.image_pairs item="image_pair"}
                    {if $image_pair}
                        {if $image_pair.image_id == 0}
                            {assign var="img_id" value=$image_pair.detailed_id}
                        {else}
                            {assign var="img_id" value=$image_pair.image_id}
                        {/if}
                        {include file="common/image.tpl" images=$image_pair object_type="detailed_product" class="sp-thumbnail" image_width=$tw_size image_height=$th_size show_thumbnail="Y" show_detailed_link=false obj_id="`$product.product_id`_`$img_id`_mini" wrap_image=false}
                    {/if}
                {/foreach}
            {/strip}

        {/if}

    </div>
</div>
{* slider end *}

<script type="text/javascript">
    {literal}

        $(document).ready(function() {

            $( '#mode_product_image' ).sliderPro({
                {/literal}
                width: 500,
                height: 550,
                slide: true,
                imageScaleMode: 'contain',
                thumbnailsPosition: 'left',
                thumbnailWidth: '80',
                thumbnailHeight: '100',
                arrows: true,
                buttons: true,
                smallSize: 500,
                mediumSize: 1000,
                largeSize: 3000,
                thumbnailArrows: true,
                autoplay: false,
                keyboard: true,
                keyboardOnlyOnFocus: true,
                {if !$product.image_pairs}
                slide: false,
                arrows: false,
                buttons: false,
                touchSwipe: false,
                {/if}
                {literal}
            });

            $(function(){
                $(".fancybox").fancybox({
                    maxWidth: 512,
                    maxHeight:768,
                    afterShow: function(){
                        var $image = $('.fancybox-image');
                        $image.CloudZoom({zoomPosition:'inside', zoomOffsetX:0});
                    },
                    beforeLoad: function(){
                        var $image = $('.fancybox-image');
                        if ($image.data('CloudZoom')) $image.data('CloudZoom').destroy();
                    },
                    beforeClose: function(){
                        var $image = $('.fancybox-image');
                        if ($image.data('CloudZoom')) $image.data('CloudZoom').destroy();
                    }
                });
            });

        });

    {/literal}
</script>