{** block-description:text_link_dropdown **}



<div class="custinfo-dropdown"><a>
    {if $block.properties.cp_mode_input_icon!=null}<i class="{$block.properties.cp_mode_input_icon}"></i>{/if}&nbsp;{$block.name}</a>
<div class="custinfo-dropdown__list">
   {assign var="text_links_id" value=$block.snapping_id}
    {if $items}
            <ul class="quick-links">
                {foreach from=$items item="menu"}
                    <li>
                        <a {if $menu.href}href="{$menu.href|fn_url}"{/if}><span>{$menu.item}</span></a> 
                    </li>
                {/foreach}
            </ul>
    {/if}

</div>
</div>