{if $items}
    <div class="tp-banner-container" id="banner_slider_{$block.snapping_id}">
        <div class="tp-banner">
            <ul>
                {foreach from=$items item="banner" key="key"}
                    <li data-transition="fade" data-slotamount="7" data-masterspeed="1000" data-thumb="{$banner.url|fn_url}" data-fstransition="fade" data-fsmasterspeed="300" data-fsslotamount="7" data-saveperformance="off">
                        {if $banner.type == "G" && $banner.main_pair.image_id}
                            {if $banner.url != ""}<a class="banner__link" href="{$banner.url|fn_url}" {if $banner.target == "B"}target="_blank"{/if}>{/if}
                            <img src="{$images_dir}/icons/spacer.gif" data-lazyload="{$banner.main_pair.icon.image_path}" data-bgposition="center top" data-kenburns="on" data-duration="8000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="100" data-bgpositionend="center bottom" >
                            {if $banner.url != ""}</a>{/if}
                        {else}
                            <div class="ty-wysiwyg-content">
                                {$banner.description nofilter}
                            </div>
                        {/if}
                    </li>
                {/foreach}
            </ul>
            <div class="tp-bannertimer">
            </div>
        </div>
    </div>
{/if}



<script type="text/javascript">
    $('.tp-banner').show().revolution({
        delay:9000,
        startwidth:960,
        startheight:300,
        hideThumbs:200,

        thumbWidth:100,
        thumbHeight:50,
        thumbAmount:4,
        
                                
        simplifyAll:"off",

        navigationType:"none",
        navigationArrows:"solo",
        navigationStyle:"preview1",

        touchenabled:"on",
        onHoverStop:"off",
        nextSlideOnWindowFocus:"on",

        swipe_threshold: 75,
        swipe_min_touches: 1,
        drag_block_vertical: false,
                                
        keyboardNavigation:"off",

        navigationHAlign:"center",
        navigationVAlign:"bottom",
        navigationHOffset:0,
        navigationVOffset:20,

        soloArrowLeftHalign:"left",
        soloArrowLeftValign:"center",
        soloArrowLeftHOffset:20,
        soloArrowLeftVOffset:0,

        soloArrowRightHalign:"right",
        soloArrowRightValign:"center",
        soloArrowRightHOffset:20,
        soloArrowRightVOffset:0,

        shadow:0,
        fullWidth:"on",
        fullScreen:"off",

        spinner:"spinner0",
                                
        stopLoop:"off",
        stopAfterLoops:-1,
        stopAtSlide:-1,

        shuffle:"off",

        autoHeight:"on",
        forceFullWidth:"on",
        
        
        
        hideThumbsOnMobile:"on",
        hideNavDelayOnMobile:1500,
        hideBulletsOnMobile:"off",
        hideArrowsOnMobile:"off",
        hideThumbsUnderResolution:0,

        hideSliderAtLimit:0,
        hideCaptionAtLimit:0,
        hideAllCaptionAtLilmit:0,
        startWithSlide:0
    });
</script>