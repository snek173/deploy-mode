{** block-description:cp_mode_tmpl_scroller **}

{if $block.properties.enable_quick_view == "Y"}
    {$quick_nav_ids = $items|fn_fields_from_multi_level:"product_id":"product_id"}
{/if}

{if $block.properties.hide_add_to_cart_button == "Y"}
        {assign var="_show_add_to_cart" value=false}
    {else}
        {assign var="_show_add_to_cart" value=true}
    {/if}
    {if $block.properties.show_price == "Y"}
        {assign var="_hide_price" value=false}
    {else}
        {assign var="_hide_price" value=true}
{/if}
{assign var="image_w" value="224"}
{assign var="image_h" value="308"}
{assign var="obj_prefix" value="`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside"  id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list">

    {foreach from=$items item="product" name="for_products"}
    
        {hook name="cp_mode_products:product_scroller_list"}
        <div class="ty-scroller-list__item cp_mode-scroller-list__item">
            {assign var="obj_id" value="scr_`$block.block_id`000`$product.product_id`"}
            <div class="product_item_img cp_mode-product_item_img">
                {include file="common/image.tpl" assign="object_img" images=$product.main_pair image_width=$image_w image_height=$image_h no_ids=true lazy_load=false}
                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{$object_img nofilter}</a>
                    <!-- <span class="wish red"> -->
                    <span class="wish">
                         {include file="addons/cp_mode/wish_button.tpl"}


                  {*include file="addons/wishlist/views/wishlist/components/add_to_wishlist.tpl" but_id="button_wishlist_`$obj_prefix``$product.product_id`" but_name="dispatch[wishlist.add..`$product.product_id`]" but_role="text"*}

                    </span>

                    {if $block.properties.cp_mode_visiblity_size == "Y"}
                    <span class="sizes" style="height:{$image_h}px;">
                        <ul>
                            <li>6</li>
                            <li>8</li>
                            <li>10</li>
                            <li>12</li>
                            <li>14</li>
                            <li>16</li>
                            <li>18</li>
                            <li>20</li>
                            <li>22</li>
                        </ul>
                    </span>
                    {/if}
                <div class="product_item_descriptions">
                {strip}
                {include file="blocks/list_templates/simple_list.tpl" product=$product show_name=true show_price=true show_add_to_cart=$_show_add_to_cart but_role="action" hide_price=$_hide_price hide_qty=true show_discount_label=true}
                {/strip}
                </div>
            </div>
        </div>
        {/hook}
    {/foreach}
</div>


{include file="common/scroller_init.tpl" prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}

