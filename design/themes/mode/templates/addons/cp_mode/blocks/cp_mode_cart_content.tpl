{** block-description:cp_mode_cart_content **}
{assign var="dropdown_id" value=$block.snapping_id}
{assign var="r_url" value=$config.current_url|escape:url}
{hook name="checkout:cp_mode_cart_content"}
    <div class="ty-dropdown-box cp_mode_shopping-bag" id="cart_status_{$dropdown_id}">
         <div id="sw_dropdown_{$dropdown_id}" class="ty-dropdown-box__title  cp_mode_shopping-bag-title cm-combination">
        <a href="{"checkout.cart"|fn_url}">
            {hook name="checkout:dropdown_title"}
                {if $smarty.session.cart.amount}
                    <i class="fa fa-shopping-bag"></i>
                    <span class="ty-minicart-title ty-hand">
                    {$smarty.session.cart.amount}

                    {*$smarty.session.cart.amount}&nbsp;{__("items")} {__("for")}&nbsp;{include file="common/price.tpl" value=$smarty.session.cart.display_subtotal*}
                    </span>
                {else}
                    <i class="fa fa-shopping-bag"></i>
                {/if}
            {/hook}
        </a>
        </div>
        <div id="dropdown_{$dropdown_id}" class="cm-popup-box ty-dropdown-box__content cp_mode_shopping-bag__content hidden">
            {hook name="checkout:cp_mode_minicart"}
                <div class="cm-cart-content {if $block.properties.products_links_type == "thumb"}cm-cart-content-thumb{/if} {if $block.properties.display_delete_icons == "Y"}cm-cart-content-delete{/if}">
                        {if $smarty.session.cart.amount || $smarty.session.cart.amount!=0}
                        <div class="cp_mode_cart_title">
                            {__("subtotal_sum")}: {include file="common/price.tpl" value=$smarty.session.cart.display_subtotal}
                        </div>
                        {/if}
                        {if $block.properties.display_bottom_buttons == "Y"}
                        <div class="cm-cart-buttons cp_mode-cart-content__buttons {if $smarty.session.cart.amount} full-cart{else} hidden{/if}">
                            <div class="ty-float-left">
                                <a href="{"checkout.cart"|fn_url}" rel="nofollow" class="ty-btn cp_mode_btn__mode cp_mode_btn__mode_click">{__("view_cart")}</a>
                            </div>
                            {if $settings.General.checkout_redirect != "Y"}
                            <div class="ty-float-right">
                                <a href="{"checkout.checkout"|fn_url}" rel="nofollow" class="ty-btn cp_mode_btn__mode">{__("checkout")}</a>
                            </div>
                            {/if}
                        </div>
                        {/if}

                        <div class="ty-cart-items">
                            {if $smarty.session.cart.amount}
                                <ul class="ty-cart-items__list">
                                    {hook name="index:cart_status"}
                                        {assign var="_cart_products" value=$smarty.session.cart.products|array_reverse:true}
                                        {foreach from=$_cart_products key="key" item="product" name="cart_products"}
                                            {hook name="checkout:minicart_product"}
                                            {if !$product.extra.parent}
                                                <li class="ty-cart-items__list-item cp_mode_cart-items__list-item">
                                                    {hook name="checkout:minicart_product_info"}
                                                    {if $block.properties.products_links_type == "thumb"}
                                                        <div class="ty-cart-items__list-item-image">
                                                            {include file="common/image.tpl" image_width="50" image_height="50" images=$product.main_pair no_ids=true}
                                                        </div>
                                                    {/if}
                                                    <div class="ty-cart-items__list-item-desc">
                                                        <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{$product.product_id|fn_get_product_name nofilter}</a>
                                                    <p>
                                                        <span>{$product.amount}</span><span>&nbsp;x&nbsp;</span>{include file="common/price.tpl" value=$product.display_price span_id="price_`$key`_`$dropdown_id`" class="none"}
                                                    </p>
                                                    </div>
                                                    {if $block.properties.display_delete_icons == "Y"}
                                                        <div class="ty-cart-items__list-item-tools cm-cart-item-delete">
                                                            {if (!$runtime.checkout || $force_items_deletion) && !$product.extra.exclude_from_calculate}
                                                                {include file="buttons/button.tpl" but_href="checkout.delete.from_status?cart_id=`$key`&redirect_url=`$r_url`" but_meta="cm-ajax cm-ajax-full-render" but_target_id="cart_status*" but_role="delete" but_name="delete_cart_item"}
                                                            {/if}
                                                        </div>
                                                    {/if}
                                                    {/hook}
                                                </li>
                                            {/if}
                                            {/hook}
                                        {/foreach}
                                    {/hook}
                                </ul>
                            {else}
                                <div class="ty-cart-items__empty cp_mode_shopping-bag__content__empty  ty-center">{__("cart_is_empty")}</div>
                            {/if}
                        </div>
                </div>
            {/hook}
        </div>
    <!--cart_status_{$dropdown_id}--></div>
{/hook}
