
        {if $auth.user_id}
            
            {assign var="custom_user_data" value=$auth.user_id|fn_get_user_info}
            <a href="{"profiles.update"|fn_url}" class="top_panel__links user-name">{$custom_user_data.firstname}&nbsp;{$custom_user_data.lastname}</a>
            <a href="{"auth.logout?redirect_url=`$return_current_url`"|fn_url}" rel="nofollow" class="text-button tdu" href="/index.php?dispatch=auth.logout&amp;redirect_url=index.php">({__("sign_out")})</a>
        {else}
            <a href="{"auth.login_form"|fn_url}"  class="top_panel__links cp_mode_btn_lg_sing_in" rel="nofollow"><i class="fa fa-user"></i> {__("login")}</a> / <a href="{"profiles.add"|fn_url}" rel="nofollow" class="cp_mode_btn_lg_rigister top_panel__links">{__("register")}</a>
            {if $settings.Security.secure_storefront != "partial"}
                <div  id="login_block{$block.snapping_id}" class="hidden" title="{__("sign_in")}">
                    <div class="ty-login-popup">
                        {include file="views/auth/login_form.tpl" style="popup" id="popup`$block.snapping_id`"}
                    </div>
                </div>
            {/if}
        {/if}
