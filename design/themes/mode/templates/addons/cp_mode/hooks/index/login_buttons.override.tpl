 <div class="buttons-container cp_login_buttons clearfix">
    {include file="buttons/login.tpl" but_name="dispatch[auth.login]" but_role="submit"}
    <a href="{"auth.recover_password"|fn_url}" class="cp_mode-password-forgot__a"  tabindex="5">{__("cp_mode_reset_password")}</a>
</div>