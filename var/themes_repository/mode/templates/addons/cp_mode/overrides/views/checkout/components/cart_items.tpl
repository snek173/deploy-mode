{capture name="cartbox"}
{if $runtime.mode == "checkout"}
    {if $cart.coupons|floatval}<input type="hidden" name="c_id" value="" />{/if}
    {hook name="checkout:form_data"}
    {/hook}
{/if}

<div id="cart_items" class="clearfix">
  
    {assign var="prods" value=false}
{if $cart_products}
        {foreach from=$cart_products item="product" key="key" name="cart_products"}
            {assign var="obj_id" value=$product.object_id|default:$key}
            {hook name="checkout:items_list"}

                {if !$cart.products.$key.extra.parent}


                    <div class="clearfix cart-line-item">
                        {if $smarty.capture.prods}{else}
                            {capture name="prods"}Y{/capture}
                        {/if}
                        {if $mode == "cart" || $show_images}
                        <div class="product-price">{include file="common/price.tpl" value=$product.display_subtotal span_id="product_subtotal_`$key`" class="price"}</div>
                        <div class="product-image cm-reload-{$obj_id}" id="product_image_update_{$obj_id}">
                            {hook name="checkout:product_icon"}
                                        <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">
                                        {include file="common/image.tpl" obj_id=$key images=$product.main_pair image_width=$settings.Thumbnails.product_cart_thumbnail_width image_height=$settings.Thumbnails.product_cart_thumbnail_height}</a>
                            {/hook}
                        <!--product_image_update_{$obj_id}--></div>
                        {/if}
                        <div class="product-description">
                            {strip}
                                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="product-title">
                                    {$product.product nofilter}
                                </a>
                            {/strip}
                            {if $smarty.capture.product_info_update|trim}
                                <div class="cm-reload-{$obj_id}" id="product_info_update_{$obj_id}">
                                    {$smarty.capture.product_info_update nofilter}
                                <!--product_info_update_{$obj_id}--></div>
                            {/if}
                            <div class="ty-cart-content__product-elem ty-cart-content__qty {if $product.is_edp == "Y" || $product.exclude_from_calculate} quantity-disabled{/if}">
                            {if $use_ajax == true && $cart.amount != 1}
                                {assign var="ajax_class" value="cm-ajax"}
                            {/if}

                            <div class="quantity cm-reload-{$obj_id}{if $settings.Appearance.quantity_changer == "Y"} changer{/if}" id="quantity_update_{$obj_id}">
                                <input type="hidden" name="cart_products[{$key}][product_id]" value="{$product.product_id}" />
                                {if $product.exclude_from_calculate}<input type="hidden" name="cart_products[{$key}][extra][exclude_from_calculate]" value="{$product.exclude_from_calculate}" />{/if}

                                <label for="amount_{$key}">{__("cp_mode_quantity_full")}:</label>
                                {if $product.is_edp == "Y" || $product.exclude_from_calculate}
                                    {$product.amount}
                                {else}
                                    {if $settings.Appearance.quantity_changer == "Y"}
                                        <div class="ty-center ty-value-changer cm-value-changer">
                                        <a class="cm-increase ty-value-changer__increase"><i class="fa fa-plus"></i></a>
                                    {/if}
                                    <input type="text" size="3" id="amount_{$key}" name="cart_products[{$key}][amount]" value="{$product.amount}" class="ty-value-changer__input cm-amount"{if $product.qty_step > 1} data-ca-step="{$product.qty_step}"{/if} />
                                    {if $settings.Appearance.quantity_changer == "Y"}
                                        <a class="cm-decrease ty-value-changer__decrease"><i class="fa fa-minus"></i></a>
                                        </div>
                                    {/if}
                                {/if}
                                {if $product.is_edp == "Y" || $product.exclude_from_calculate}
                                    <input type="hidden" name="cart_products[{$key}][amount]" value="{$product.amount}" />
                                {/if}
                                {if $product.is_edp == "Y"}
                                    <input type="hidden" name="cart_products[{$key}][is_edp]" value="Y" />
                                {/if}
                            <!--quantity_update_{$obj_id}--></div>
                            </div>
                            {if $product.product_options}
                                <div class="cm-reload-{$obj_id} ty-cart-content__options" id="options_update_{$obj_id}">
                                {include file="views/products/components/product_options.tpl" product_options=$product.product_options product=$product name="cart_products" id=$key location="cart" disable_ids=$disable_ids form_name="checkout_form"}
                                <!--options_update_{$obj_id}--></div>
                            {/if}

                            <div class="cart-item-btn-w">
                                {strip}
                                    {if !$product.exclude_from_calculate}
                                        <a class="{$ajax_class} ty-cart-content__product-delete ty-delete-big" href="{"checkout.delete?cart_id=`$key`&redirect_mode=`$runtime.mode`"|fn_url}" data-ca-target-id="cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart" title="{__("remove")}">{__("cp_mode_remove")}
                                        </a>
                                    {/if}
                                {/strip}
                                <a class="ty-btn ty-btn__secondary cm-external-click cp_mode-btn-update_cart " data-ca-external-click-id="button_cart">{__("cp_mode_update")}</a>
                               
       

                                <span class="sku {if !$product.product_code} hidden{/if}" id="sku_{$key}">
                                {__("sku")}: <span class="cm-reload-{$obj_id}" id="product_code_update_{$obj_id}">{$product.product_code}<!--product_code_update_{$obj_id}--></span>
                                </span>
                            </div>
                        <!--quantity_update_{$obj_id}--></div>
                            
                        
                            
                        </div>

   

                {/if}
            {/hook}
        {/foreach}
        {/if}

        {hook name="checkout:extra_list"}
        {/hook}


<!--cart_items--></div>

{/capture}
{include file="common/mainbox_cart.tpl" title=__("cart_items") content=$smarty.capture.cartbox}
