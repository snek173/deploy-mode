{if $layout_data.layout_width != "fixed"}
    {if $parent_grid.width > 0}
        {$fluid_width = fn_get_grid_fluid_width($layout_data.width, $parent_grid.width, $grid.width)}
    {else}
        {$fluid_width = $grid.width}
    {/if}
{/if}

{if $grid.status == "A" && $content}
    

    {if ( $grid.position !== 'FOOTER' || $addons.cp_mode_class.e_footer == 'Y' ) && $grid.parent_id == 0}
        {if $grid.alpha}<div class="container-fluid__row{if $grid.full_width == "Y"} container-fluid__row--full-width{/if}{if $grid.full_width == "W"} container-fluid__row--no-limit{/if}{if $grid.full_width != "N"} {$grid.user_class}{/if}">{/if}
    {/if}
    
    {if $grid.alpha}<div class="{if $layout_data.layout_width != "fixed"}row-fluid {else}row{/if}">{/if}
        {$width = $fluid_width|default:$grid.width}
        <div class="span{$width}{if $grid.offset} offset{$grid.offset}{/if}{if $grid.full_width == "N"} {$grid.user_class}{/if}" >
                {$content nofilter}
        </div>
    {if $grid.omega}</div>{/if}
        
    {if ( $grid.position !== 'FOOTER' || $addons.cp_mode_class.e_footer == 'Y' ) && $grid.parent_id == 0}
        {if $grid.omega}</div>{/if}
    {/if}
{/if}