{assign var="columns" value=16}
{if !$wishlist_is_empty}

    {script src="js/tygh/exceptions.js"}

    {assign var="show_hr" value=false}
    {assign var="location" value="cart"}
{/if}
{if $products}
    {include file="addons/cp_mode/blocks/list_templates/wishlist_view.tpl" 
        columns=$columns
        show_empty=true
        show_name=true 
        show_old_price=false 
        show_price=true 
        show_clean_price=false 
        show_list_discount=false
        no_pagination=true
        no_sorting=true
        show_sku=true
        show_add_to_cart=true
        is_wishlist=true}
{else}
    <div class="ty-column16">
        <div class="ty-product-empty">
            <span class="ty-product-empty__text">{__("empty")}</span>
        </div>
    </div>
{/if}

{if !$wishlist_is_empty}
    <div class="buttons-container ty-wish-list__buttons cp_mode-wish-list__buttons">
        {include file="buttons/button.tpl" but_text=__("clear_wishlist") but_href="wishlist.clear" but_meta="ty-btn__tertiary cp_mode-reset-wishlist"}
        {include file="buttons/continue_shopping.tpl" but_href=$continue_url|fn_url but_role="text"}
    </div>
{else}
    <div class="buttons-container ty-wish-list__buttons ty-wish-list__continue">
        {include file="buttons/continue_shopping.tpl" but_href=$continue_url|fn_url but_role="text"}
    </div>
{/if}

{capture name="mainbox_title"}{__("wishlist_content")}{/capture}
