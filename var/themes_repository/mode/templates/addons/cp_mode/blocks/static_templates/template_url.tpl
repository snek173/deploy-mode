{** block-description:template_url **}
{if $block.properties.cp_mode_tsetting == "Q"}
<a href="{$block.properties.cp_mode_input_url}" class="cp_mode_TLink"><i class="{$block.properties.cp_mode_input_icon}"></i></a>
{else if $block.properties.cp_mode_tsetting == "T"}
<a href="{$block.properties.cp_mode_input_url}" class="cp_mode_TLink">{$block.properties.cp_mode_input_name_url}</a>
{else}
<a href="{$block.properties.cp_mode_input_url}" class="cp_mode_TLink"><i class="{$block.properties.cp_mode_input_icon}"></i> {$block.properties.cp_mode_input_name_url}</a>
{/if}