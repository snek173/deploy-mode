{** block-description:cp_mode_search **}

<div class="cp_mode_search_block">
    <a href="#" class="cp_mode_fa_csearch"><i class="cp_mode_fa {$block.properties.cp_mode_input_icon}"></i></a>
    <div class="cp_mode_search_block_form hidden">
        <div class="cp_mode_search_block_form_body">
            <form action="{""|fn_url}" name="search_form" method="get">
                <input type="hidden" name="subcats" value="Y" />
                <input type="hidden" name="pcode_from_q" value="Y" />
                <input type="hidden" name="pshort" value="Y" />
                <input type="hidden" name="pfull" value="Y" />
                <input type="hidden" name="pname" value="Y" />
                <input type="hidden" name="pkeywords" value="Y" />
                <input type="hidden" name="search_performed" value="Y" />
                {hook name="search:additional_fields"}{/hook}
                {strip}
                    {if $settings.General.search_objects}
                        {assign var="search_title" value=__("search")}
                    {else}
                        {assign var="search_title" value=__("cp_mode_search_products")}
                    {/if}
                    <input type="text" name="q" value="{$search.q}" id="search_input{$smarty.capture.search_input_id}" title="{$search_title}" class="cp_mode_search-block__input cm-hint" />
                    {if $settings.General.search_objects}
                        {include file="buttons/magnifier.tpl" but_name="search.results" alt=__("search")}
                    {else}
                        {include file="buttons/magnifier.tpl" but_name="products.search" alt=__("search")}
                    {/if}
                {/strip}
                {capture name="search_input_id"}{$block.snapping_id}{/capture}
            <a href="#" class="cp_mode_fa_csearch_close"><i class="fa fa-times"></i></a>
            </form>
        </div>
    </div>
</div>
    
{literal}
<script type="text/javascript">
    $(document).ready(function() {
        $("a.cp_mode_fa_csearch").click(function(event){
            $(this).addClass("active");
            $(".cp_mode_search_block .cp_mode_search_block_form").addClass("active").show().animate({width: "100%"}, 400 );
            return false;
        });

        $("a.cp_mode_fa_csearch_close").click(function(event){
            $(this).removeClass("active");
            $(".cp_mode_search_block .cp_mode_search_block_form").removeClass("active").hide().animate({width: "0"}, 0 );
            return false;
        });
    });
</script>
{/literal}