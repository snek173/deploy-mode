{hook name="blocks:cp_mode_topmenu_dropdown"}

{assign var="vendor_info" value=0|fn_blocks_get_vendor_info}

{if $block.properties.cp_mode_visiblity_logo == "Y"}
<div class="logo">
    <a href="{""|fn_url}">
        <img src="{$vendor_info.logos.theme.image.image_path}" width="{$vendor_info.logos.theme.image.image_x}" height="{$vendor_info.logos.theme.image.image_y}" alt="{$vendor_info.logos.theme.image.alt}" class="cp_mode_logo">
    </a>
</div>
{/if}


{if $items}
    <ul class="cp_mode-menu_horizontal cp_mode-menu__items">
        {hook name="blocks:cp_mode_topmenu_dropdown_top_menu"}
        {foreach from=$items item="item1" name="item1"}
            {assign var="item1_url" value=$item1|fn_form_dropdown_object_link:$block.type}
            {assign var="unique_elm_id" value=$item1_url|md5}
            {assign var="unique_elm_id" value="topmenu_`$block.block_id`_`$unique_elm_id`"}

            <li class="cp_mode-menu_horizontal-fli cp_mode-menu__item{if !$item1.$childs} cp_mode-menu__item-nodrop{else} cm-menu-item-responsive{/if}{if $item1.active || $item1|fn_check_is_active_menu_item:$block.type} cp_mode-menu__item-active{/if}{if $item1.class} {$item1.class}{/if}">
                    <a {if $item1_url} href="{$item1_url}"{/if} class="cp_mode-menu__item-link">
                        {$item1.$name}
                    </a>
                    {if $item1.$childs}
                        <a class="cp_mode-menu__item-toggles">
                            <i class="fa fa-angle-down"></i>
                        </a>
                    {/if}
                {if $item1.$childs}

                    {if !$item1.$childs|fn_check_second_level_child_array:$childs}
                    {* Only two levels. Vertical output *}
                        <div class="cp_mode-menu_horizontal-menu__submenu  cp_mode-menu__submenu">
                            <ul class="cp_mode-menu__submenu-items cp_mode-menu__submenu-items-simple cm-responsive-menu-submenu">
                                {hook name="blocks:topmenu_dropdown_2levels_elements"}
        
                                {foreach from=$item1.$childs item="item2" name="item2"}
                                    {assign var="item_url2" value=$item2|fn_form_dropdown_object_link:$block.type}
                                    <li class="cp_mode-menu_horizontal-menu__submenu-simples cp_mode-menu__submenu-item{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} cp_mode-menu__submenu-item-active{/if}{if $item2.class} {$item2.class}{/if}">
                                        <a class="cp_mode-menu__submenu-link" {if $item_url2} href="{$item_url2}"{/if}>{$item2.$name}</a>
                                    </li>
                                {/foreach}
                                {if $item1.show_more && $item1_url}
                                    <li class="cp_mode-menu__submenu-item cp_mode-menu__submenu-alt-link">
                                        <a href="{$item1_url}"
                                           class="cp_mode-menu__submenu-alt-link">{__("text_topmenu_view_more")}</a>
                                    </li>
                                {/if}

                                {/hook}
                            </ul>
                        </div>
                    {* END  Only two levels. Vertical output------------------------------------------------------------------------------------------- *}
                    {else}
                         <div class="cp_mode-menu_horizontal-menu__submenu  cp_mode-menu__submenu">
                             {hook name="blocks:cp_mode_topmenu_dropdown_3levels_cols"}
                                    <div class="cp_mode-menu__submenu-items_3levels_cols">
                                        {foreach from=$item1.$childs item="item2" name="item2"}
                                        <div class="cp_mode-menu__submenu-items_3levels_cols_block">
                                            <div class="cp_mode-top-mine__submenu-col">
                                                {assign var="item2_url" value=$item2|fn_form_dropdown_object_link:$block.type}
                                                <div class="cp_mode-menu__submenu-item-header{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} cp_mode-menu__submenu-item-header-active{/if}{if $item2.class} {$item2.class}{/if}">
                                                    <a{if $item2_url} href="{$item2_url}"{/if} class="cp_mode-menu__submenu-link">{$item2.$name}</a>
                                                </div>
                                            </div>
                                            <div class="cp_mode-menu__submenu-items_3levels_cols__submenu_block">
                                                <ul class="cp_mode-menu__submenu-list_3levels_cols__submenu cm-responsive-menu-submenu">
                                                    {if $item2.$childs}
                                                        {hook name="blocks:topmenu_dropdown_3levels_col_elements"}
                                                        {foreach from=$item2.$childs item="item3" name="item3"}
                                                            {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
                                                            <li class="cp_mode-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item3.class} {$item3.class}{/if}">
                                                                <a{if $item3_url} href="{$item3_url}"{/if}
                                                                        class="ty-menu__submenu-link">{$item3.$name}</a>
                                                            </li>
                                                        {/foreach}
                                                        
                                                        {if $item2.show_more && $item2_url}
                                                            <li class="cp_mode-menu__submenu-list_3levels_cols__submenu cp_mode-menu__submenu-alt-link">
                                                                <a href="{$item2_url}"
                                                                   class="cp_mode-menu__submenu-link">{__("text_topmenu_view_more")}</a>
                                                            </li>
                                                        {/if}
                                                        {/hook}
                                                    {/if}
                                                </ul>
                                            </div>
                                        </div>
                                        {/foreach}
                                        {if $item1.show_more && $item1_url}
                                            <div class="cp_mode-menu__submenu-dropdown-bottom">
                                                <a href="{$item1_url}">{__("text_topmenu_more", ["[item]" => $item1.$name])}</a>
                                            </div>
                                        {/if}
                                    </div>
                                {/hook}
                         </div>
                    <!-- asdsa -->
                    {/if}

                {/if}
            </li>
        {/foreach}

        {/hook}
    </ul>
{/if}

{/hook}
