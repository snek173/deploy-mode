{if $products}

    {script src="js/tygh/exceptions.js"}
    

    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}
    
    {if !$no_sorting}
        {include file="views/products/components/sorting.tpl"}
    {/if}

    {if !$show_empty}
        {split data=$products size=$columns|default:"2" assign="splitted_products"}
    {else}
        {split data=$products size=$columns|default:"2" assign="splitted_products" skip_complete=true}
    {/if}

    {math equation="100 / x" x=$columns|default:"2" assign="cell_width"}
    {if $item_number == "Y"}
        {assign var="cur_number" value=1}
    {/if}

    {* FIXME: Don't move this file *}
    {script src="js/tygh/product_image_gallery.js"}

    {if $settings.Appearance.enable_quick_view == 'Y'}
        {$quick_nav_ids = $products|fn_fields_from_multi_level:"product_id":"product_id"}
    {/if}
   




        {foreach from=$products item=product key=key name="products"}
            {assign var="obj_id" value=$product.product_id}
            {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
            {include file="common/product_data.tpl" product=$product min_qty=true}
        {hook name="wishlist:items_list"}
        {if !$wishlist.products.$key.extra.parent}

        {if $show_hr}
        <hr />
        {else}
            {assign var="show_hr" value=true}
        {/if}

        <div class="product-container clearfix">

            {assign var="form_open" value="form_open_`$obj_id`"}
            {$smarty.capture.$form_open nofilter}         

            <div class="product-image">
                <span class="cm-reload-{$key}" id="product_image_update_{$key}">
                    <input type="hidden" name="appearance[wishlist]" value="1" />
                    <a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{include file="common/image.tpl" image_width=$settings.Thumbnails.product_lists_thumbnail_width image_height=$settings.Thumbnails.product_lists_thumbnail_height obj_id=$key images=$product.main_pair object_type="product" show_thumbnail="Y"}</a>
                <!--product_image_update_{$key}--></span>
            </div>
            <div class="product-description">
                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="product-title">{$product.product|unescape}</a>
                    {if $show_sku}
                        <div class="cp_mode_sku-item cp_mode_sku-item-wishlist ty-control-group ty-sku-item cm-hidden-wrapper{if !$product.product_code} hidden{/if}" id="sku_update_{$obj_prefix}{$obj_id}">
                            <input type="hidden" name="appearance[show_sku]" value="{$show_sku}" />
                            <label class="ty-control-group__label" id="sku_{$obj_prefix}{$obj_id}">{__("sku")}:</label>
                            <span class="ty-control-group__item cm-reload-{$obj_prefix}{$obj_id}" id="product_code_{$obj_prefix}{$obj_id}">{$product.product_code}<!--product_code_{$obj_prefix}{$obj_id}--></span>
                        </div>
                    {/if}
                <div class="ty-grid-list__price cp_mode-grid-list__price__w1 {if $product.price == 0}ty-grid-list__no-price{/if}">
                    {assign var="old_price" value="old_price_`$obj_id`"}
                    {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}

                    {assign var="price" value="price_`$obj_id`"}
                    {$smarty.capture.$price nofilter}

                    {assign var="clean_price" value="clean_price_`$obj_id`"}
                    {$smarty.capture.$clean_price nofilter}

                    {assign var="list_discount" value="list_discount_`$obj_id`"}
                    {$smarty.capture.$list_discount nofilter}
                </div>
                    {if !$smarty.capture.capt_options_vs_qty}
                    <div class="cp_mode-content-desc">
                        <div class="ty-product-list__option">
                            {assign var="product_options" value="product_options_`$obj_id`"}
                            {$smarty.capture.$product_options nofilter}
                        </div>

                        {assign var="product_amount" value="product_amount_`$obj_id`"}
                        {$smarty.capture.$product_amount nofilter}
                        
                        <div class="ty-product-list__qty">
                            {assign var="qty" value="qty_`$obj_id`"}
                            {$smarty.capture.$qty nofilter}
                        </div>
                    </div>
                    {/if}
                    {assign var="advanced_options" value="advanced_options_`$obj_id`"}
                    {$smarty.capture.$advanced_options nofilter}

                    {assign var="min_qty" value="min_qty_`$obj_id`"}
                    {$smarty.capture.$min_qty nofilter}

                    {assign var="product_edp" value="product_edp_`$obj_id`"}
                    {$smarty.capture.$product_edp nofilter}

                {if $show_add_to_cart}
                    <div class="button-container cp_mode-button-container-btnwishlist">
                        {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                        {$smarty.capture.$add_to_cart nofilter}
                    </div>
                {/if}

                {*include file="blocks/list_templates/simple_list.tpl" obj_id=$key product=$product show_sku=true show_old_price=true show_price=true show_list_discount=true show_discount_label=true show_product_amount=true show_product_options=true show_min_qty=true show_edp=true show_add_to_cart=true but_role="action"*}
                
                <div class="cart-item-btn-w">
                    <a href="{"wishlist.delete?cart_id=`$product.cart_id`"|fn_url}">{__(cp_mode_remove)}</span></a>
        
                </div>
            </div>
            {assign var="form_close" value="form_close_`$obj_id`"}
            {$smarty.capture.$form_close nofilter}
        </div>
        {/if}
        {/hook}
        {/foreach}
    



    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

{/if}

{capture name="mainbox_title"}{$title}{/capture} 